/**
 * Tempstep Plugin
 *
 */

/* global document, window */


( function( document, window ) {
    "use strict";

    // Copied from core impress.js. Good candidate for moving to src/lib/util.js.
    var triggerEvent = function( el, eventName, detail ) {
        var event = document.createEvent( "CustomEvent" );
        event.initCustomEvent( eventName, true, true, detail );
        el.dispatchEvent( event );
    };

    var activeStep = null;
    document.addEventListener( "impress:stepenter", function( event ) {
        activeStep = event.target;
    }, false );

    var tempstep = function( event ) {
        if ( ( !event ) || ( !event.target ) ) {
            return;
        }

        var step = event.target;
				var data = step.dataset;
				var numberOfTempSteps = parseInt(data.tempstepNumber,10);
				if(data.tempstepSkip) {
					var list = data.tempstepSkip.split(";");
				} else {
					var list = [];
				}
				var skipList = [];
				for(var i=0;i<list.length;i++) {
					var times = list[i].match(/(\d+)/g);
					if(! times) {
						continue;
					}
					if(times.length === 1) {
						skipList.push(parseInt(times[0]));
					} else if(times.length === 2 && parseInt(times[0]) <= parseInt(times[1])) {
						for(var j=parseInt(times[0]);j<=parseInt(times[1]);j++) {
							skipList.push(j);
						}
					}
				}
				
				if( ! numberOfTempSteps ) {
					return;
				}
				
				var onTempstep = step.classList.value.match(/on-tempstep-(\d+)/);
				var tempStepCounter;
				if(! onTempstep ) {
					tempStepCounter = 0;
				}	else {
					tempStepCounter = parseInt(onTempstep[1],10);
				}
								
				step.classList.remove("on-tempstep-" + tempStepCounter);
				if(event.detail.reason === "next") {
					do {
						tempStepCounter++;
					} while(skipList.includes(tempStepCounter));
				}	else if(event.detail.reason == "prev") {
					do {
						tempStepCounter--;
					} while(skipList.includes(tempStepCounter));
				} else {
					return;
				}
				step.classList.add("on-tempstep-" + tempStepCounter);
				
				var tempsteps = step.querySelectorAll(".tempstep")
				
				for(var i = 0; i < tempsteps.length; i++) {
					setTempClasses(tempsteps[i], tempStepCounter);
					if(isTempstepActive(tempsteps[i], tempStepCounter, numberOfTempSteps)) {
						tempsteps[i].classList.add("tempstep-active");
					} else {
						tempsteps[i].classList.remove("tempstep-active");
					}
				}
				
				if(data.tempstepGlobalClasses) {
						setGlobalTempClasses(data.tempstepGlobalClasses,tempStepCounter);
				}				
				
				if(tempStepCounter === 0 || tempStepCounter > numberOfTempSteps) {
					return;
				} else {
					triggerEvent( step, "impress:substep:stepleaveaborted",
                              { reason: "prev", substep : null} );
					return false;
				}
    };
	
	
		var setGlobalTempClasses = function(globalTempClassesList, tempStepCounter) {
			var cL = document.body.classList;
			var cLnew = [];
			var regex = RegExp('tempGClass-[A-Za-z0-9]*', 'g');
			for(var i=0;i<cL.length;i++) {
				if(! regex.test(cL[i])) {
					cLnew.push(cL[i]);
				}
			}
			document.body.classList = "";
			for(var i=0;i<cLnew.length;i++) {
				document.body.classList.add(cLnew[i]);
			}

			var list = globalTempClassesList.split(";");
			for(var i=0;i<list.length;i++) {
				var llist = list[i].split(":");
				var times = llist[0].match(/(\d+)/g);
				if(! times) {
					
				} else {
					if(times.length === 1 && parseInt(times[0]) === tempStepCounter) {
						for(var j=1;j<llist.length;j++) {
							document.body.classList.add("tempGClass-" + llist[j]);
						}
					} else if(times.length === 2 && parseInt(times[0]) <= tempStepCounter && tempStepCounter <= parseInt(times[1])) {
						for(var j=1;j<llist.length;j++) {
							document.body.classList.add("tempGClass-" + llist[j]);
						}
					}
				}
			}					
			return true;
		}
	
	
		var setTempClasses = function(tempstep, tempStepCounter) {
			var tempClassList = tempstep.dataset.tempstepClasses;
			if(tempClassList) {
				var cL = tempstep.classList;
				var cLnew = [];
				var regex = RegExp('tempClass-[A-Za-z0-9]*', 'g');
				for(var i=0;i<cL.length;i++) {
					if(! regex.test(cL[i])) {
						cLnew.push(cL[i]);
					}
				}
				tempstep.classList = "";
				for(var i=0;i<cLnew.length;i++) {
					tempstep.classList.add(cLnew[i]);
				}

				var list = tempClassList.split(";");
				for(var i=0;i<list.length;i++) {
					var llist = list[i].split(":");
					var times = llist[0].match(/(\d+)/g);
					if(! times) {
						
					} else {
						if(times.length === 1 && parseInt(times[0]) === tempStepCounter) {
							for(var j=1;j<llist.length;j++) {
								tempstep.classList.add("tempClass-" + llist[j]);
							}
						} else if(times.length === 2 && parseInt(times[0]) <= tempStepCounter && tempStepCounter <= parseInt(times[1])) {
							for(var j=1;j<llist.length;j++) {
								tempstep.classList.add("tempClass-" + llist[j]);
							}
						}
					}
				}
			}					
			return true;
		}
		
		var isTempstepActive = function(tempstep, tempStepCounter, numberOfTempSteps) {
			var activeList = tempstep.dataset.tempstepList;
			if(activeList) {
				var list = activeList.split(";");
				for(var i=0;i<list.length;i++) {
					var times = list[i].match(/(\d+)/g);
					if(! times) {
						return false;
					}
					if(times.length === 1 && parseInt(times[0]) === tempStepCounter) {
						return true
					} else if(times.length === 2 && parseInt(times[0]) <= tempStepCounter && tempStepCounter <= parseInt(times[1])) {
						return true;
					}
				}
				return false;
			}			
			var activeFrom = tempstep.dataset.tempstepFrom || 0;
			var activeUntil = tempstep.dataset.tempstepUntil || (numberOfTempSteps+1);
			return (activeFrom <= tempStepCounter && tempStepCounter <= activeUntil)
		}

    // Register the plugin to be called in pre-stepleave phase.
    // The weight makes this plugin run before other preStepLeave plugins.
    window.impress.addPreStepLeavePlugin( tempstep, 1 );

    // When entering a step, in particular when re-entering, make sure that all substeps are hidden
    // at first
    document.addEventListener( "impress:stepenter", function( event ) {
        var step = event.target;
				var data = step.dataset;
				var numberOfTempSteps = parseInt(data.tempstepNumber,10);
				
				if( ! numberOfTempSteps ) {
					return;
				}
				
				var onTempstep = step.classList.value.match(/on-tempstep-(\d+)/);
				var tempStepCounter;
				if(! onTempstep ) {
					tempStepCounter = 0;
				}	else {
					tempStepCounter = parseInt(onTempstep[1],10);
				}
						
				if(tempStepCounter === 0 || tempStepCounter > numberOfTempSteps) {
					step.classList.remove("on-tempstep-" + tempStepCounter);
					if(tempStepCounter === 0) {
						tempStepCounter++;
					}	else if(tempStepCounter > numberOfTempSteps) {
						tempStepCounter = numberOfTempSteps;
					}
					step.classList.add("on-tempstep-" + tempStepCounter);
					
					var tempsteps = step.querySelectorAll(".tempstep")
					
					for(var i = 0; i < tempsteps.length; i++) {
						setTempClasses(tempsteps[i], tempStepCounter);
						if(isTempstepActive(tempsteps[i], tempStepCounter, numberOfTempSteps)) {
							tempsteps[i].classList.add("tempstep-active");
						} else {
							tempsteps[i].classList.remove("tempstep-active");
						}
						//var activeFrom = tempsteps[i].dataset.tempstepFrom || 0;
						//var activeUntil = tempsteps[i].dataset.tempstepUntil || (numberOfTempSteps+1);
						//if(activeFrom <= tempStepCounter && tempStepCounter <= activeUntil) {
						//	tempsteps[i].classList.add("tempstep-active");
						//} else {
						//	tempsteps[i].classList.remove("tempstep-active");
						//}
					}
        }
				
    }, false );

} )( document, window );



/*
*
* Custom Keys
*
*/

(function ( document, window ) {
    'use strict';
    // wait for impress.js to be initialized
    document.addEventListener("impress:init", function (event) {
        var api = event.detail.api;
        var gc = api.lib.gc;
        
        gc.addEventListener(document, "keyup", function ( event ) {
			// 'o' springt direkt zur Überblick-Folie
			if ( event.keyCode === 79) {
				api.goto("Overview")
			}
			// 't' springt direkt zum Anfang
			if ( event.keyCode === 84) {
				api.goto("Title")
			}
        }, false);

    }, false);
})(document, window);